# -*- encoding: utf-8 -*-

"""
Use the file to load and initialize objects to be used by the
application. Currently, the app is kept as simple as possible and no
webhooks are yet defined. However, one may directly change the
base URL with any webhook of choice.
"""

import os

from aiogram import Bot, Dispatcher, types

bot = Bot(token = os.getenv("AUT0CBOT_TOKEN"), parse_mode = types.ParseMode.HTML)
dp = Dispatcher(bot) # dispatcher object
