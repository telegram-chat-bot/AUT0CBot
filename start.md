<b>👋 Hello {username},</b>

My name is 🤓 <a href = "https://zenithclown.github.io/">Debmalya Pramanik</a> and I'm really excited that you have found my <a href = "https://gitlab.com/telegram-chat-bot/AUT0CBot">🤖 <code>AUT0CBot</code></a>. This is a application under testing chat bot 🧮 developed using <a href = "https://github.com/aiogram/aiogram">aiogram</a>. Mostly, the bot does not do anything, unless you want to get references to:

  * 📔 Learn how to build a <code>bot</code> using <a href = "https://core.telegram.org/bots/api">Telegram Bot API</a>, or
  * 📔 To learn about <a href = "https://tinyurl.com/AUT0CBot">API Endpoints</a> and get started using <a href = "https://www.postman.com/">postman</a>.
  * 📔 Check the actual 🤖 <a href = "https://t.me/dPramanikBot">dPramanik</a> for all other information.
