# -*- encoding: utf-8 -*-

"""
Application Under Testing (AUT-0) Chat Bot

A simple telegram based chatbot for user interactions and updates,
created using `aiogram` package. To start using this package, simply
create an Environment Variable (`AUT0CBOT_TOKEN`) which is directly
accessed via the process, or set the token in `TOKEN` file (not
secured).
"""

from aiogram import executor, types
from aiogram.types import ReplyKeyboardMarkup

from loader import * # noqa: F403 # pylint: disable=unused-import


@dp.message_handler(commands = ["start", "help"])
async def cmd_start(message: types.Message) -> bool:
    # this handler will be called when user send `/start` or `/help` command
    markup = ReplyKeyboardMarkup(resize_keyboard = True)
    markup.row("test-user", "Debmalya Pramanik")

    await message.answer(open("./start.md", encoding = "utf-8").read().strip().format(
        username = message.from_user.full_name
    ))
    return True


@dp.message_handler()
async def echo(message: types.Message) -> bool:
    # ! default `fallback` handler
    # this response is sent out when the message is not configured
    markup = ReplyKeyboardMarkup(resize_keyboard = True)
    markup.row("test-user", "Debmalya Pramanik")

    await message.answer(open("./default.md", encoding = "utf-8").read().strip().format(
        username = message.from_user.full_name,
        messageID = message.message_id,
        message = message.text
    ))
    return True

if __name__ == "__main__":
    executor.start_polling(dp, skip_updates = True)
