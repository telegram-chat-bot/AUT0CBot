👋 Hello {username},

<i>I'm still learning</i>! This bot is not intended for a full fledged chat conversation. Type <code>/start</code> to check the documentation. However, your message with the below content is received (and logged for internal training and quality monitoring purposes):

"{{ <b>Message ID:</b> {messageID} }} with {{ <b>Content:</b> <i>{message}</i> }}"

Check the actual 🤖 <a href = "https://t.me/dPramanikBot">dPramanik</a> for all other information.